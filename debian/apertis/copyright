Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: LGPL-2.0-or-later

Files: .gitlab-ci/*
Copyright: no-info-found
License: Expat

Files: debian/*
Copyright: 2007-2010 Ross Burton <ross@debian.org>
            2010-2016 Andreas Henriksson <andreas@fatal.se>
License: LGPL-2+

Files: examples/*
Copyright: no-info-found
License: BSD-2-clause

Files: examples/browse.js
 examples/last-change.js
Copyright: 2016, Jens Georg <mail@jensge.org>
License: BSD-2-clause

Files: examples/test-browsing.c
 examples/test-proxy.c
 examples/test-server.c
Copyright: 2006-2008, OpenedHand Ltd.
License: LGPL-2.1

Files: examples/test-context-manager.c
Copyright: 2022, Jens Georg <mail@jensge.org>
License: Expat

Files: examples/test-introspection.c
Copyright: 2007, Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 2007, OpenedHand Ltd.
License: LGPL-2+

Files: examples/test-white-list.c
Copyright: 2013, Intel Corporation.
License: LGPL-2.1

Files: libgupnp/*
Copyright: 2006-2008, OpenedHand Ltd.
License: LGPL-2.1

Files: libgupnp/gupnp-acl-private.h
 libgupnp/gupnp-acl.c
 libgupnp/gupnp-acl.h
Copyright: 2012-2014, Jens Georg <mail@jensge.org>
License: LGPL-2.1

Files: libgupnp/gupnp-connman-manager.c
 libgupnp/gupnp-connman-manager.h
Copyright: 2012, Intel Corporation.
 2009, Nokia Corporation.
License: LGPL-2.1

Files: libgupnp/gupnp-context-filter.c
 libgupnp/gupnp-context-filter.h
Copyright: 2013, Intel Corporation.
License: LGPL-2.1

Files: libgupnp/gupnp-context-manager.c
Copyright: 2013, Intel Corporation.
 2009, Nokia Corporation.
 2006-2008, OpenedHand Ltd.
License: LGPL-2.1

Files: libgupnp/gupnp-context-manager.h
 libgupnp/gupnp-context.c
 libgupnp/gupnp-simple-context-manager.c
 libgupnp/gupnp-simple-context-manager.h
 libgupnp/gupnp-unix-context-manager.c
 libgupnp/gupnp-unix-context-manager.h
 libgupnp/gupnp-xml-doc.c
 libgupnp/gupnp-xml-doc.h
Copyright: 2009, 2011, Nokia Corporation.
 2006-2008, OpenedHand Ltd.
License: LGPL-2.1

Files: libgupnp/gupnp-linux-context-manager.c
 libgupnp/gupnp-uuid.h
Copyright: 2011, 2015, 2022, Jens Georg
License: LGPL-2.1

Files: libgupnp/gupnp-linux-context-manager.h
 libgupnp/gupnp-service-private.h
Copyright: 2011, 2019, 2021, Jens Georg.
License: LGPL-2.1

Files: libgupnp/gupnp-network-manager.c
Copyright: 2009, Nokia Corporation.
License: LGPL-2+

Files: libgupnp/gupnp-network-manager.h
Copyright: 2009, Nokia Corporation.
License: LGPL-2.1

Files: libgupnp/gupnp-resource-factory-private.h
 libgupnp/gupnp-resource-factory.c
 libgupnp/gupnp-resource-factory.h
 libgupnp/gupnp-service-introspection.c
 libgupnp/gupnp-service-introspection.h
 libgupnp/gupnp-types-private.h
 libgupnp/gupnp-types.c
 libgupnp/gupnp-types.h
Copyright: 2007, Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 2006, 2007, OpenedHand Ltd.
License: LGPL-2.1

Files: libgupnp/gupnp-service-action.c
 libgupnp/gupnp-service-proxy-private.h
Copyright: no-info-found
License: LGPL-2.1

Files: libgupnp/gupnp-service-proxy-action-private.h
Copyright: 2018, 2019, The GUPnP maintainers.
License: LGPL-2.1

Files: libgupnp/gupnp-windows-context-manager.c
Copyright: 2009, Nokia Corporation.
 2009, 2010, Jens Georg
 2006-2008, OpenedHand Ltd.
License: LGPL-2+

Files: libgupnp/gupnp-windows-context-manager.h
Copyright: 2009, Nokia Corporation.
 2009, 2010, Jens Georg
 2006-2008, OpenedHand Ltd.
License: LGPL-2.1

Files: tests/*
Copyright: 2011, 2019, 2021, Jens Georg.
License: LGPL-2.1

Files: tests/test-bugs.c
Copyright: 2013, Intel Corporation.
License: LGPL-2.1

Files: tests/test-context-filter.c
Copyright: 2011, 2015, 2022, Jens Georg
License: LGPL-2.1

Files: tests/test-context.c
Copyright: 2012, Nokia.
License: LGPL-2.1

Files: tests/test-service.c
Copyright: no-info-found
License: LGPL-2.1

Files: tools/*
Copyright: 2008, OpenedHand Ltd
 2008, Intel Corporation
License: GPL-2

Files: vala/*
Copyright: 2012-2014, Jens Georg <mail@jensge.org>
License: LGPL-2.1

Files: tests/data/*
Copyright: 2006, 2007, 2008 OpenedHand Ltd.
 2007 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 2009,2010,2011,2012,2013,2014,2015 Jens Georg
 2009,2011,2012 Nokia Corporation.
 2008,2012,2013 Intel Corporation. All rights reserved.
License: LGPL-2+
